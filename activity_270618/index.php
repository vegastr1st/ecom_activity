<?php
    $category = "";
    $name = "";
    $desc = "";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GETTING STARTED WITH BRACKETS</title>
        <meta name="description" content="An interactive getting started guide for Brackets.">
        <link rel="stylesheet" href="main.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <br>
        <div class="container">
            <button type="button" class="btn btn-primary btn-lg">Product</button>

            <button type="button" class="btn btn-primary btn-lg">Category</button>
        </div>
        
        <br>
        
        <div class="container">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td><input type="button" name="delete" value="delete"></td>
                    </tr>
                    
                </tbody>
            </table>
            
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Add
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php
            $nameErr = $catErr = "";
            $category = $cat = $desc = "";
            
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                
                if (empty($_POST["category"])) {
                    $catErr = "Category is required";
                } else {
                    $category = test_input($_POST["category"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                        $catErr = "Only letters and white space allowed"; 
                    }
                }
                
                if (empty($_POST["name"])) {
                    $nameErr = "Name is required";
                } else {
                    $name = test_input($_POST["name"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                        $nameErr = "Only letters and white space allowed"; 
                    }
                }

                if (empty($_POST["desc"])) {
                    $desc = "";
                } else {
                    $desc = test_input($_POST["desc"]);
                }

            }

            function test_input($data) {
              $data = trim($data);
              $data = stripslashes($data);
              $data = htmlspecialchars($data);
              return $data;
            }
            ?>
            
            
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
              Category:
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Category" name="category" value="<?php echo $category;?>">
              <br><br>
              Name:
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Name" name="name" value="<?php echo $name;?>">
              <br><br>
              Comment:
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Desc" name="desc" value="<?php echo $desc;?>"></textarea>
              <br><br>

              <br><br>
              <input type="submit" name="submit" value="Submit">  
            </form>
                        </div>

                    </div>
                </div>
            </div>
            
            
        </div>
        
        <?php
            echo $category;
            echo "<br>";
            echo $name;
            echo "<br>";
            echo $desc;
            echo "<br>";
            ?>
    </body>
</html>
